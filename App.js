import React, { Component } from 'react';
import { Text, View } from 'react-native';
import RNPrintExample from './Components/RnPrintExample';

class App extends Component {
  render() {
    return <RNPrintExample />;
  }
}

export default App;
